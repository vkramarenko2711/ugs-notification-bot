package com.vkram.bot

import java.util.*

data class Lecture(val message: String, val date: Date)

fun utc(year: Int, month: Int, date: Int, hour: Int, minute: Int) : Date {
    val now = Calendar.getInstance()

    now.set(year, month - 1, date, hour-3, minute-5, 0)
    println(now.timeZone)

    return now.time
}