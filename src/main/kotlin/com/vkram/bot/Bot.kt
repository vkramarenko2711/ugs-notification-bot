package com.vkram.bot

import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.dispatcher.command

import java.util.*


fun main() {
    val rw = "SAT R&W Lecture starting: https://us02web.zoom.us/j/87881882954?pwd=cFpEVDJFdXFHcWFYc1RNb3hpb0R6UT09"
    val math = "SAT Math Lecture starting: https://us02web.zoom.us/j/85331043875?pwd=dmozM01qODZpNHNBR1JVUC9SQW1HUT09"
    val toefl = "TOEFL Lecture starting: https://us02web.zoom.us/j/88116950177?pwd=aTRjVG9CaHNoVi9SSDd6MkpjYi9Ndz09"
    val bookClub = "Book Club starting: https://us02web.zoom.us/j/82934100002?pwd=SFZmeUxsRWtmZkZ0RFZkbVBpdXVzdz09"
    val officeHours =
        "Office hours starting: https://us02web.zoom.us/j/87064991454?pwd=SFFoYUp4YzFKV1FSSm0rZFZGVzdrZz09"

    val lectures = arrayOf(
        Lecture(math, utc(2021, 7, 19, 11, 0)),
        Lecture(rw, utc(2021, 7, 19, 18, 0)),
        Lecture(toefl, utc(2021, 7, 20, 10, 0)),
        Lecture(math, utc(2021, 7, 14, 11, 0)),
        Lecture(rw, utc(2021, 7, 14, 18, 0)),
        Lecture(toefl, utc(2021, 7, 15, 10, 0)),
        Lecture(math, utc(2021, 7, 16, 11, 0)),
        Lecture(officeHours, utc(2021, 7, 16, 14, 30)),
        Lecture(bookClub, utc(2021, 7, 17, 11, 0)),
        Lecture(officeHours, utc(2021, 7, 18, 14, 30))
    )
    var chatId: Long = -1

    val bot = bot {
        token = "1879185423:AAEf0wkpWllve41oq2F8_iNZ8lOCklxTZJo"
        dispatch {
            command("register") {
                println("register ${message.chat.id}")
                chatId = message.chat.id
            }

            command("text") {
                bot.sendMessage(ChatId.fromId(chatId), "I'm bot")
            }
        }
    }

    for (l in lectures) {
        println("$l")
        val task: TimerTask = object : TimerTask() {
            override fun run() {
                println("message ${l.message}")
                if (chatId != -1L) bot.sendMessage(ChatId.fromId(chatId), l.message)
            }
        }

        Timer().schedule(task, l.date, 604800000)
    }


    bot.startPolling()
}



